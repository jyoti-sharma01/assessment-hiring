Sample CSV Parse project.

Getting started:

1. Install node 16.6.2 and higher.
2. Run ```npm install``` in the root repo.
3. Run ```npm run parse-sample``` command to parse ```sample.csv``` file 


Dev notes: 

1. By default we parse sample.csv, in case if you want to use custom file you can use:
```node index.js ./<relativePathToCustomFile>```
2. CSV headers ```USER_ID,FIRST_AND_LAST_NAME,VERSION,INSURANCE_COMPANY``` are required.