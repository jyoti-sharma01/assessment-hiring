const fs = require("fs");
const path = require("path");
const csv = require("fast-csv");

const inputFilePath = process.argv[2] || "./sample.csv";
const options = { headers: true };
const insuranceCompanies = {};

const parseInsuranceCompanyData = (insuranceCompanyData) =>
  [
    ...new Map(
      insuranceCompanyData
        .sort(
          ({ VERSION: versionFirst }, { VERSION: versionSecond }) =>
            versionFirst - versionSecond
        )
        .map((item) => [item.USER_ID, item])
    ).values(),
  ].sort(
    ({ FIRST_AND_LAST_NAME: nameFirst }, { FIRST_AND_LAST_NAME: nameSecond }) =>
      nameFirst.localeCompare(nameSecond)
  );

const parseInsuranceCompanyName = (insuranceCompanyName) =>
  `${insuranceCompanyName.toLowerCase().replace(" ", "-")}.csv`;

fs.createReadStream(path.resolve(__dirname, inputFilePath))
  .pipe(csv.parse(options))
  .on("data", (row) => {
    if (!insuranceCompanies[row.INSURANCE_COMPANY])
      insuranceCompanies[row.INSURANCE_COMPANY] = [];
    insuranceCompanies[row.INSURANCE_COMPANY].push(row);
  })
  .on("end", () => {
    Object.keys(insuranceCompanies).forEach((insuranceCompany) =>
      csv
        .write(
          parseInsuranceCompanyData(insuranceCompanies[insuranceCompany]),
          options
        )
        .pipe(
          fs.createWriteStream(
            path.resolve(
              __dirname,
              "insurance",
              parseInsuranceCompanyName(insuranceCompany)
            )
          )
        )
    );
  });

//  console.log(parseInsuranceCompanyData());
